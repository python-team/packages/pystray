Source: pystray
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Claudius Heine <ch@denx.de>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dunst <!nocheck>,
 gir1.2-ayatanaappindicator3-0.1 <!nocheck>,
 gir1.2-glib-2.0 <!nocheck>,
 gir1.2-gtk-3.0 <!nocheck>,
 libglib2.0-tests <!nocheck>,
 python3-all,
 python3-pil <!nocheck>,
 python3-setuptools,
 python3-six <!nocheck>,
 python3-sphinx <!nodoc>,
 python3-xlib <!nocheck>,
 xauth <!nocheck>,
 xvfb <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://github.com/moses-palmer/pystray
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/pystray.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pystray

Package: python-pystray-doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Section: doc
Build-Profiles: <!nodoc>
Description: Python library to create system tray icons (documentation)
 This package contains the documentation for pystray, a Python library to
 create system tray icons.
 .
 Pystray allows specifying an icon, a title and a callback for when the icon is
 activated. The icon and title can be changed after the icon has been created,
 and the visibility of the icon can be toggled.
 .
 It supports Linux under Xorg, GNOME and Ubuntu, macOS and Windows.

Package: python3-pystray
Architecture: all
Depends:
 gir1.2-ayatanaappindicator3-0.1,
 gir1.2-glib-2.0,
 gir1.2-gtk-3.0,
 python3-gi,
 ${misc:Depends},
 ${python3:Depends},
Suggests: python-pystray-doc
Description: Python library to create system tray icons (Python 3)
 This package installs pystray for Python 3. Pystray is a Python library to
 create system tray icons.
 .
 Pystray allows specifying an icon, a title and a callback for when the icon is
 activated. The icon and title can be changed after the icon has been created,
 and the visibility of the icon can be toggled.
 .
 It supports Linux under Xorg, GNOME and Ubuntu, macOS and Windows.
